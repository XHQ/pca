#pragma once

#include "../../publicH.h"

class PCA
{
public:
	PCA();
	~PCA();
	void ConstructData(const char *faceList);
	//根据数据源的不同,重载方法,创建相同形式的mfaceData数据
	void ComputeEigenvector();
	void Print();

	double *meigenvalues;//mfaceCount-1
	double **meigenvectors;//3*mfacePoints rows, mfaceCount-1 columns
	vtkDoubleArray**mfaceData;//mfaceCount rows, 3*mfacePoints columns
private:
	int mfaceCount;
	int mfacePoints;
};

void IgnoreComments(ifstream& f);
void ReadInString(ifstream& f, char* s);
