#include "pca.h"

#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>

#include <fstream>
using namespace std;

PCA::PCA()
	:meigenvalues(NULL),meigenvectors(NULL)
{
}

PCA::~PCA()
{
	delete []meigenvalues;
	meigenvalues = NULL;

	for (int i = 0; i < mfaceCount-1; i++)
	{
		delete [] meigenvectors[i];
		meigenvectors[i] = NULL;
	}
	free(meigenvectors);
	meigenvectors = NULL;
}


void PCA::ConstructData(const char *faceList)
{
	ifstream f;
	f.open(faceList);
	assert(f != NULL);

	IgnoreComments(f);f>>mfaceCount;IgnoreComments(f);
	IgnoreComments(f);f>>mfacePoints;IgnoreComments(f);

	//construct data, stored in variable mfaceData
	//each row is a sample
	mfaceData =(vtkDoubleArray**)malloc(3*mfacePoints*sizeof(vtkDoubleArray*));
	for (int i = 0; i < 3*mfacePoints; i++)
	{
		mfaceData[i] = vtkDoubleArray::New();
		char name[25];
		itoa(i, name, 10);
		mfaceData[i]->SetNumberOfComponents(1);
		mfaceData[i]->SetName(name);
	}

	int i, j, k;
	char faceFileName[256];
	vtkPolyDataReader*reader = NULL;
	vtkPolyData	*data = NULL;
	vtkPoints	*facePoints = NULL; 

	ofstream f2;
	f2.open("./data/mfaceData.txt");

	IgnoreComments(f);
	for (i = 0; i < mfaceCount; i++)
	{
		ReadInString(f, faceFileName);

		reader = vtkPolyDataReader::New();
		reader->SetFileName(faceFileName);
		reader->Modified();
		reader->Update();

		data = vtkPolyData::New();
		data->DeepCopy(reader->GetOutput());
		data->Modified();
		data->Update();

		facePoints = data->GetPoints();

		for (j = 0; j < mfacePoints; j++)
		{
			double coordinate[3];
			facePoints->GetPoint(j, coordinate);
			//printf("i = %d, (%f %f %f)\n", i, coordinate[0], coordinate[1], coordinate[2]);
			for (k = 0; k < 3; k++)
			{
				mfaceData[3*j + k]->InsertNextValue(coordinate[k]);
				f2 << coordinate[k] << " ";
			}
		}
		f2 << ";" << endl;

		data->Delete();
		reader->Delete();
	}

	f2.close();
	f.close();

	cout << "人脸数据保存在 \"./data/mfaceData.txt\" 中" << endl;
}

void PCA::ComputeEigenvector()
{
	int i;

	//fDataTable:load data into table(mfaceCount rows, 3*mfacePoints columns)
	//each row is a sample
	vtkSmartPointer<vtkTable>fDataTable
		= vtkSmartPointer<vtkTable>::New();
	for (i = 0; i < 3*mfacePoints; i++)
	{
		fDataTable->AddColumn(mfaceData[i]);
	}

	//释放mfaceData占用的内存，也可以放在析构函数中
	for (i = 0; i < mfaceCount; i++)
	{
		mfaceData[i]->Delete();
	}
	free(mfaceData);
	mfaceData = NULL;

	vtkSmartPointer<vtkPCAStatistics> pcaStatistics =
		vtkSmartPointer<vtkPCAStatistics>::New();

#if VTK_MAJOR_VERSION <= 5
	pcaStatistics->SetInput( vtkStatisticsAlgorithm::INPUT_DATA, fDataTable );
#else
	pcaStatistics->SetInputData( vtkStatisticsAlgorithm::INPUT_DATA, fDataTable );
#endif

	for (i = 0; i < 3*mfacePoints ; i++)
	{
		char name[25];
		itoa(i, name, 10);
		pcaStatistics->SetColumnStatus(name, 1);
	}
	pcaStatistics->RequestSelectedColumns();
	pcaStatistics->SetDeriveOption(true);
	pcaStatistics->Update();

	/***********Eigenvalues************/
	vtkSmartPointer<vtkDoubleArray> eigenvalues =
		vtkSmartPointer<vtkDoubleArray>::New();
	pcaStatistics->GetEigenvalues(eigenvalues);

	meigenvalues = new double[mfaceCount-1];assert(meigenvalues != NULL);
	for(i = 0; i < mfaceCount-1/*eigenvalues->GetNumberOfTuples()*/; i++)
	{
		meigenvalues[i] = eigenvalues->GetValue(i);
	}

	/***********Eigenvectors************/
	vtkSmartPointer<vtkDoubleArray>eigenvectors
		= vtkSmartPointer<vtkDoubleArray>::New();
	pcaStatistics->GetEigenvectors(eigenvectors);

	meigenvectors = (double**)malloc((mfaceCount-1)*sizeof(double*));assert(meigenvectors != NULL);
	//meigenvectors = new double* [mfaceCount-1];
	for (i = 0; i < mfaceCount-1/*eigenvectors->GetNumberOfTuples()*/; i++)
	{
		meigenvectors[i] = new double[3*mfacePoints];assert(meigenvectors[i] != NULL);
		eigenvectors->GetTuple(i, meigenvectors[i]);
	}
}

void PCA::Print()
{
	int i, j;
	ofstream f;
	f.open("./data/meigenvectors.txt");

	f << "eigenvalues:" << endl;
	for(int i = 0; i < mfaceCount - 1; i++)
	{
		f << i << " = " << meigenvalues[i] << endl;
	}
	f << endl;

	f << "eigenvectors:" << endl;
	for(i = 0; i < mfaceCount - 1; i++)
	{
		f << i << " : ";
		for(j = 0; j < 3*mfacePoints; j++)
		{
			f << meigenvectors[i][j] << " ";
		}
		f << endl;
	}

	f.close();

	cout << "特征向量保存在 \"./data/meigenvectors.txt\" 中" << endl;
}

void IgnoreComments(ifstream& f)
{
	f.ignore(65536,'\n');
}

void ReadInString(ifstream& f, char* s)
{
	char buf[256];
	f.getline(buf,255,'\n');
	strcpy(s, buf);
	//s = buf;
}