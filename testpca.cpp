#include "publicH.h"
#include "./src/pca/pca.h"
#include <malloc.h>
//#include <crtdbg.h>

int main()
{
	PCA *vertexPca  = new PCA;

	vertexPca->ConstructData("./data/facelist.txt");
	vertexPca->ComputeEigenvector();
	vertexPca->Print();

	printf("PCA compute finish...\n");

	//_CrtSetDbgFlag(_CRTDBG_LEAK_CHECK_DF | _CRTDBG_ALLOC_MEM_DF);
	//_CrtDumpMemoryLeaks();
	getchar();
	return 0;
}