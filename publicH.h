#pragma once

#include <vtkVersion.h>

#include "vtkSmartPointer.h"
#include "vtkPolyDataReader.h"
#include "vtkPolyData.h"
#include "vtkDoubleArray.h"
#include "vtkPoints.h"
#include "vtkPCAStatistics.h"
#include "vtkTable.h"

#include <assert.h>
